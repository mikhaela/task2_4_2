﻿using CmsWebApi.Controllers;
using CmsWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace CmsWebApi.Tests
{
    public class RelatedPagesTests
    {
        [Fact]
        public void CreateRelatedPage_ValidRelatedPage_ReturnsCreatedAtRoute()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test 2 a",
                UrlName = "page2"
            };
            var pages = new List<Page>();
            pages.Add(page1);
            pages.Add(page2);
            var rp = new RelatedPage()
            {
                PageId = page1.Id,
                AssociatedPageId = page2.Id
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindPage(page1.Id)).Returns(page1);
            mockRepo.Setup(x => x.FindPage(page2.Id)).Returns(page2);
            mockRepo.Setup(x => x.FindRelatedPage(rp.PageId, rp.AssociatedPageId)).Returns(new RelatedPage());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.CreateRelatedPage(rp);

            Assert.IsType(typeof(CreatedAtRouteResult), res);
            CreatedAtRouteResult actualResult = res as CreatedAtRouteResult;
            Assert.Equal("GetRelatedPage", actualResult.RouteName);
            Assert.Equal(rp.PageId, actualResult.RouteValues["pageId"]);
            Assert.Equal(rp.AssociatedPageId, actualResult.RouteValues["associatedPageId"]);
            Assert.Equal("Cms", actualResult.RouteValues["controller"]);
            mockRepo.Verify(r => r.Add(rp));
        }

        [Fact]
        public void CreateRelatedPage_InvalidRelatedPage_ReturnsBadRequest()
        {
            var rp = new RelatedPage()
            {
                PageId = 1,
                AssociatedPageId = 2
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindPage(1)).Returns(new Page());
            mockRepo.Setup(x => x.FindPage(2)).Returns(new Page());
            mockRepo.Setup(x => x.FindRelatedPage(rp.PageId, rp.AssociatedPageId)).Returns(new RelatedPage());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.CreateRelatedPage(rp);

            Assert.IsType(typeof(BadRequestResult), res);
        }

        [Fact]
        public void GetRelatedPageById_ValidId_ReturnsRelatedPage()
        {
            var rp = new RelatedPage()
            {
                PageId = 1,
                AssociatedPageId = 2
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindRelatedPage(rp.PageId, rp.AssociatedPageId)).Returns(rp);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetRelatedPageById(rp.PageId, rp.AssociatedPageId);

            Assert.IsType(typeof(ObjectResult),res);
            Assert.Equal(rp, (res as ObjectResult).Value);
        }

        [Fact]
        public void GetRelatedPageById_InvalidId_ReturnsNotFound()
        {
            var rp = new RelatedPage()
            {
                PageId = 1,
                AssociatedPageId = 2
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindRelatedPage(rp.PageId, rp.AssociatedPageId)).Returns(new RelatedPage());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetRelatedPageById(rp.PageId, rp.AssociatedPageId);

            Assert.IsType(typeof(NotFoundResult), res);
        }

        [Fact]
        public void DeleteRelatedPage_ValidId_ReturnsNoContent()
        {
            var rp = new RelatedPage()
            {
                PageId = 1,
                AssociatedPageId = 2
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindRelatedPage(rp.PageId, rp.AssociatedPageId)).Returns(rp);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.DeleteRelatedPage(rp.PageId, rp.AssociatedPageId);

            Assert.IsType(typeof(NoContentResult), res);
            mockRepo.Verify(x => x.RemoveRelatedPage(rp.PageId, rp.AssociatedPageId));
        }

        [Fact]
        public void DeleteRelatedPage_InvalidId_ReturnsNotFound()
        {
            var rp = new RelatedPage()
            {
                PageId = 1,
                AssociatedPageId = 2
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindRelatedPage(rp.PageId, rp.AssociatedPageId)).Returns(new RelatedPage());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.DeleteRelatedPage(rp.PageId, rp.AssociatedPageId);

            Assert.IsType(typeof(NotFoundResult), res);
        }

        [Fact]
        public void GetRelatedPages_NonEmptyRepo_ReturnsRelatedPages()
        {
            var relatedPages = new List<RelatedPage>();
            for (int i = 0; i < 5; ++i)
            {
                relatedPages.Add(new RelatedPage()
                {
                    PageId = i,
                    AssociatedPageId = i+1
                });
            }
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetRelatedPages()).Returns(relatedPages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetRelatePages();

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<RelatedPage> actualResult = (IEnumerable<RelatedPage>)objResult.Value;
            Assert.Equal(relatedPages, actualResult.ToList());
            mockRepo.Verify(r => r.GetRelatedPages());
        }

        [Fact]
        public void GetRelatedPages_EmptyRepo_ReturnsEmptyCollection()
        {
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetRelatedPages()).Returns(new List<RelatedPage>());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetRelatePages();

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<RelatedPage> actualResult = (IEnumerable<RelatedPage>)objResult.Value;
            Assert.Equal(new List<RelatedPage>(), actualResult.ToList());
            mockRepo.Verify(r => r.GetRelatedPages());
        }
    }
}
