﻿using CmsWebApi.Controllers;
using CmsWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace CmsWebApi.Tests
{
    public class PagesTests
    {
        [Fact]
        public void GetPageById_ValidId_ReturnsPage()
        {
            var page = new Page()
            {
                Id = 1,
                Title = "Test",
                UrlName = "test",
                Description = "It's a test",
                Content = "<h1>Hello</h1>",
                AddedDate = DateTime.Now
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(repo => repo.FindPage(page.Id)).Returns(page);
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.GetPageById(page.Id);
            Assert.IsType(typeof(ObjectResult), result);
            ObjectResult objResult = result as ObjectResult;

            Assert.Equal(page, objResult.Value);
        }

        [Fact]
        public void GetPageById_InvalidId_ReturnsNotFound()
        {
            var page = new Page()
            {
                Id = 1,
                Title = "Test",
                UrlName = "test",
                Description = "It's a test",
                Content = "<h1>Hello</h1>",
                AddedDate = DateTime.Now
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(repo => repo.FindPage(page.Id)).Returns(page);
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.GetPageById(++page.Id);
            Assert.IsType(typeof(NotFoundResult), result);
            NotFoundResult actualResult = result as NotFoundResult;

            Assert.Equal(new NotFoundResult().StatusCode, actualResult.StatusCode);
        }

        [Fact]
        public void DeletePage_ValidId_ReturnsNoContentResult()
        {
            var page = new Page()
            {
                Id = 1,
                Title = "Test",
                UrlName = "test",
                Description = "It's a test",
                Content = "<h1>Hello</h1>",
                AddedDate = DateTime.Now
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(repo => repo.FindPage(page.Id)).Returns(page);
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.DeletePage(page.Id);
            Assert.IsType(typeof(NoContentResult), result);
        }

        [Fact]
        public void DeletePage_InvalidId_ReturnsNotFound()
        {
            var mockRepo = new Mock<IPageRepository>();
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.DeletePage(1);
            Assert.IsType(typeof(NotFoundResult), result);
        }

        [Fact]
        public void CreatePage_ValidPage_ReturnsCreatedAtRoute()
        {
            var page = new Page()
            {
                Id = 1,
                Title = "Test",
                UrlName = "test",
                Description = "It's a test",
                Content = "<h1>Hello</h1>",
                AddedDate = DateTime.Now
            };

            var mockRepo = new Mock<IPageRepository>();
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.CreatePage(page);

            Assert.IsType(typeof(CreatedAtRouteResult), result);
            CreatedAtRouteResult actualResult = result as CreatedAtRouteResult;
            Assert.Equal("GetPage", actualResult.RouteName);
            Assert.Equal(page.Id, actualResult.RouteValues["id"]);
            Assert.Equal("Cms", actualResult.RouteValues["controller"]);
            mockRepo.Verify(r => r.Add(page));
        }

        [Fact]
        public void CreatePage_NullPage_ReturnsBadRequest()
        {
            Page page = null;
            var mockRepo = new Mock<IPageRepository>();
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.CreatePage(page);

            Assert.IsType(typeof(BadRequestResult), result);
        }

        [Fact]
        public void CreatePage_PageWithNonUniqueUrl_ReturnsBadRequest()
        {

            var page = new Page()
            {
                Id = 1,
                UrlName = "test"
            };
            var allPages = new List<Page>();
            allPages.Add(page);
            var duplicate = new Page()
            {
                Id = 2,
                UrlName = "test",
            };
            var repo = new Mock<IPageRepository>();
            repo.Setup(r => r.GetPages()).Returns(allPages);
            var controller = new CmsController(repo.Object);

            IActionResult result = controller.CreatePage(duplicate);

            Assert.IsType(typeof(BadRequestResult), result);
        }

        [Fact]
        public void CreatePage_PageWithEmptyUrl_ReturnsBadRequest()
        {
            var page = new Page()
            {
                Id = 1,
                Title = "Test",
                UrlName = "",
                Description = "It's a test",
                Content = "<h1>Hello</h1>",
                AddedDate = DateTime.Now
            };

            var mockRepo = new Mock<IPageRepository>();
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.CreatePage(page);

            Assert.IsType(typeof(BadRequestResult), result);
        }

        [Fact]
        public void CreatePage_PageWithNullUrl_ReturnsBadRequest()
        {
            var page = new Page()
            {
                Id = 1,
                Title = "Test",
                UrlName = null,
                Description = "It's a test",
                Content = "<h1>Hello</h1>",
                AddedDate = DateTime.Now
            };

            var mockRepo = new Mock<IPageRepository>();
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.CreatePage(page);

            Assert.IsType(typeof(BadRequestResult), result);
        }

        [Fact]
        public void UpdatePage_ValidPage_ReturnsNoContent()
        {
            var page = new Page()
            {
                Id = 1,
                Title = "Test",
                UrlName = "test",
                Description = "It's a test",
                Content = "<h1>Hello</h1>"
            };
            var updatedPage = new Page()
            {
                Id = 1,
                Title = "Test Updated",
                UrlName = "test-u",
                Description = "It's a test for update method",
                Content = "<h1>Hello U</h1>"
            };
            var allPages = new List<Page>();
            allPages.Add(page);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(r => r.FindPage(1)).Returns(page);
            mockRepo.Setup(r => r.GetPages()).Returns(allPages);
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.UpdatePage(updatedPage.Id,updatedPage);
            mockRepo.Verify(r => r.Update(updatedPage));
            Assert.IsType(typeof(NoContentResult), result);
        }

        [Fact]
        public void UpdatePage_PageWithNonUniqueUrl_ReturnsBadRequest()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test",
                UrlName = "some-url",
                Description = "It's a test",
                Content = "<h1>Hello</h1>"
            };

            var page2 = new Page()
            {
                Id = 2,
                Title = "Test",
                UrlName = "test",
                Description = "It's a test",
                Content = "<h1>Hello</h1>"
            };
            var updatedPage2 = new Page()
            {
                Id = 2,
                Title = "Test Updated",
                UrlName = "some-url",
                Description = "It's a test for update method",
                Content = "<h1>Hello U</h1>"
            };
            var allPages = new List<Page>();
            allPages.Add(page1);
            allPages.Add(page2);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(r => r.FindPage(page2.Id)).Returns(page2);
            mockRepo.Setup(r => r.GetPages()).Returns(allPages);
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.UpdatePage(updatedPage2.Id, updatedPage2);
            
            Assert.IsType(typeof(BadRequestResult), result);
        }

        [Fact]
        public void UpdatePage_InvalidId_ReturnsNotFound()
        {
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test",
                UrlName = "test",
                Description = "It's a test",
                Content = "<h1>Hello</h1>"
            };
            var updatedPage2 = new Page()
            {
                Id = 2,
                Title = "Test Updated",
                UrlName = "some-url",
                Description = "It's a test for update method",
                Content = "<h1>Hello U</h1>"
            };
            var allPages = new List<Page>();
            allPages.Add(page2);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(r => r.FindPage(page2.Id)).Returns(page2);
            mockRepo.Setup(r => r.GetPages()).Returns(allPages);
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.UpdatePage(++updatedPage2.Id, updatedPage2);

            Assert.IsType(typeof(NotFoundResult), result);
        }

        [Fact]
        public void UpdatePage_PageWithEmptyUrl_ReturnsBadRequest()
        {
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test",
                UrlName = "test",
                Description = "It's a test",
                Content = "<h1>Hello</h1>"
            };
            var updatedPage2 = new Page()
            {
                Id = 2,
                Title = "Test Updated",
                UrlName = "",
                Description = "It's a test for update method",
                Content = "<h1>Hello U</h1>"
            };
            var allPages = new List<Page>();
            allPages.Add(page2);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(r => r.FindPage(page2.Id)).Returns(page2);
            mockRepo.Setup(r => r.GetPages()).Returns(allPages);
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.UpdatePage(updatedPage2.Id, updatedPage2);

            Assert.IsType(typeof(BadRequestResult), result);
        }

        [Fact]
        public void UpdatePage_PageWithNullUrl_ReturnsBadRequest()
        {
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test",
                UrlName = "test",
                Description = "It's a test",
                Content = "<h1>Hello</h1>"
            };
            var updatedPage2 = new Page()
            {
                Id = 2,
                Title = "Test Updated",
                UrlName = null,
                Description = "It's a test for update method",
                Content = "<h1>Hello U</h1>"
            };
            var allPages = new List<Page>();
            allPages.Add(page2);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(r => r.FindPage(page2.Id)).Returns(page2);
            mockRepo.Setup(r => r.GetPages()).Returns(allPages);
            var controller = new CmsController(mockRepo.Object);

            IActionResult result = controller.UpdatePage(updatedPage2.Id, updatedPage2);

            Assert.IsType(typeof(BadRequestResult), result);
        }

        [Fact]
        public void GetPages_NoFilters_ReturnsPages()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test 2",
                UrlName = "page2"
            };
            var page3 = new Page()
            {
                Id = 3,
                Title = "Test 3",
                UrlName = "page3"
            };
            var pages = new List<Page>();
            pages.Add(page1);
            pages.Add(page2);
            pages.Add(page3);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(pages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages();

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<Page> actualResult = (IEnumerable<Page>)objResult.Value;
            Assert.Equal(pages, actualResult.ToList());
        }

        [Fact]
        public void GetPages_WhenRepoEmpty_ReturnsEmptyCollection()
        {
            var pages = new List<Page>();
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(pages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages();

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<Page> actualResult = (IEnumerable<Page>)objResult.Value;
            Assert.Equal(pages, actualResult.ToList());
        }

        [Fact]
        public void GetPages_ExistentTitle_ReturnsFilteredPages()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test 2",
                UrlName = "page2"
            };
            var page3 = new Page()
            {
                Id = 3,
                Title = "Test 3",
                UrlName = "page3"
            };
            var pages = new List<Page>();
            pages.Add(page1);
            pages.Add(page2);
            pages.Add(page3);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(pages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("1");

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<Page> actualResult = (IEnumerable<Page>)objResult.Value;
            pages = pages.Where(x => x.Title.Contains("1")).ToList();
            Assert.Equal(pages, actualResult.ToList());
        }

        [Fact]
        public void GetPages_NonExistentTitle_ReturnsEmptyCollection()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test 2",
                UrlName = "page2"
            };
            var page3 = new Page()
            {
                Id = 3,
                Title = "Test 3",
                UrlName = "page3"
            };
            var pages = new List<Page>();
            pages.Add(page1);
            pages.Add(page2);
            pages.Add(page3);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(pages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("5");

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<Page> actualResult = (IEnumerable<Page>)objResult.Value;
            Assert.Equal(new List<Page>(), actualResult.ToList());
        }

        [Fact]
        public void GetPages_NegativeOffset_ReturnsBadRequest()
        {
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(new List<Page>());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("",-5);

            Assert.IsType(typeof(BadRequestResult), res);
        }

        [Fact]
        public void GetPages_OffsetLargerThanNumOfPages_ReturnsBadRequest()
        {
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(new List<Page>());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("", 5);

            Assert.IsType(typeof(BadRequestResult), res);
        }

        [Fact]
        public void GetPages_NegativePagination_ReturnsBadRequest()
        {
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(new List<Page>());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("",0,-5);

            Assert.IsType(typeof(BadRequestResult), res);
        }

        [Fact]
        public void GetPages_PaginationCountLargerThanPageCount_ReturnsPages()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test 2",
                UrlName = "page2"
            };
            var page3 = new Page()
            {
                Id = 3,
                Title = "Test 3",
                UrlName = "page3"
            };
            var pages = new List<Page>();
            pages.Add(page1);
            pages.Add(page2);
            pages.Add(page3);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(pages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("",0,10);

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<Page> actualResult = (IEnumerable<Page>)objResult.Value;
            Assert.Equal(pages, actualResult.ToList());
        }

        [Fact]
        public void GetPages_ZeroPaginationCount_ReturnsBadRequest()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test 2",
                UrlName = "page2"
            };
            var pages = new List<Page>();
            pages.Add(page1);
            pages.Add(page2);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(pages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("", 0, 0);

            Assert.IsType(typeof(BadRequestResult), res);
        }

        [Fact]
        public void GetPages_OffsetEqualToPageCount_ReturnsBadRequest()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test 2",
                UrlName = "page2"
            };
            var pages = new List<Page>();
            pages.Add(page1);
            pages.Add(page2);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(pages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("",pages.Count());

            Assert.IsType(typeof(BadRequestResult), res);
        }

        [Fact]
        public void GetPages_ValidOffset_ReturnsPages()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test 2",
                UrlName = "page2"
            };
            var page3 = new Page()
            {
                Id = 3,
                Title = "Test 3",
                UrlName = "page3"
            };
            var pages = new List<Page>();
            pages.Add(page1);
            pages.Add(page2);
            pages.Add(page3);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(pages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("", 2);

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<Page> actualResult = (IEnumerable<Page>)objResult.Value;
            Assert.Equal(pages.Skip(2), actualResult.ToList());
        }

        [Fact]
        public void GetPages_ValidPagination_ReturnsPages()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test 2",
                UrlName = "page2"
            };
            var page3 = new Page()
            {
                Id = 3,
                Title = "Test 3",
                UrlName = "page3"
            };
            var pages = new List<Page>();
            pages.Add(page1);
            pages.Add(page2);
            pages.Add(page3);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(pages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("", 0, 2);

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<Page> actualResult = (IEnumerable<Page>)objResult.Value;
            Assert.Equal(pages.Take(2), actualResult.ToList());
        }

        [Fact]
        public void GetPages_TitleFilterOffsetPageCount_ReturnsPages()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var page2 = new Page()
            {
                Id = 2,
                Title = "Test 2 a",
                UrlName = "page2"
            };
            var page3 = new Page()
            {
                Id = 3,
                Title = "Test 3 a",
                UrlName = "page3"
            };
            var pages = new List<Page>();
            pages.Add(page1);
            pages.Add(page2);
            pages.Add(page3);
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetPages()).Returns(pages);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetPages("a", 1, 1);

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<Page> actualResult = (IEnumerable<Page>)objResult.Value;
            var expectedPages = pages.Where(x => x.Title.Contains("a")).Skip(1).Take(1);
            Assert.Equal(expectedPages, actualResult.ToList());
        }
    }
}
