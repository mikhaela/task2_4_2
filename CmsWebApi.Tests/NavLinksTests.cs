﻿using CmsWebApi.Controllers;
using CmsWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;


namespace CmsWebApi.Tests
{
    public class NavLinksTests
    {
        [Fact]
        public void CreateNavLink_ValidNavLink_ReturnsCreatedAtRoute()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var link = new NavLink()
            {
                Id = 1,
                PageId = page1.Id,
                Title = "Link for Test 1"
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindPage(page1.Id)).Returns(page1);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.CreateNavLink(link);

            Assert.IsType(typeof(CreatedAtRouteResult), res);
            CreatedAtRouteResult actualResult = res as CreatedAtRouteResult;
            Assert.Equal("GetNavLink", actualResult.RouteName);
            Assert.Equal(link.Id, actualResult.RouteValues["id"]);
            Assert.Equal("Cms", actualResult.RouteValues["controller"]);
            mockRepo.Verify(r => r.Add(link));
        }

        [Fact]
        public void CreateNavLink_NavLinkWithEmptyTitle_ReturnsBadRequest()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var link = new NavLink()
            {
                Id = 1,
                PageId = page1.Id,
                Title = ""
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindPage(page1.Id)).Returns(page1);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.CreateNavLink(link);

            Assert.IsType(typeof(BadRequestResult), res);
        }

        [Fact]
        public void CreateNavLink_NavLinkWithNullTitle_ReturnsBadRequest()
        {
            var page1 = new Page()
            {
                Id = 1,
                Title = "Test 1",
                UrlName = "page1"
            };
            var link = new NavLink()
            {
                Id = 1,
                PageId = page1.Id,
                Title = null
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindPage(page1.Id)).Returns(page1);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.CreateNavLink(link);

            Assert.IsType(typeof(BadRequestResult), res);
        }

        [Fact]
        public void CreateNavLink_NavLinkWithInvalidPageId_ReturnsBadRequest()
        {
            var link = new NavLink()
            {
                Id = 1,
                PageId = 1,
                Title = "Title"
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindPage(1)).Returns(new Page());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.CreateNavLink(link);

            Assert.IsType(typeof(BadRequestResult), res);
        }

        [Fact]
        public void GetNavLinkById_ValidId_ReturnsNavLink()
        {
            var link = new NavLink()
            {
                Id = 1,
                PageId = 1,
                Title = "Title"
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindNavLink(link.Id)).Returns(link);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetNavLinkById(link.Id);

            Assert.IsType(typeof(ObjectResult), res);
            Assert.Equal(link, (res as ObjectResult).Value);
            mockRepo.Verify(r => r.FindNavLink(link.Id));
        }

        [Fact]
        public void GetNavLinkById_InvalidId_ReturnsNotFound()
        {
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindNavLink(1)).Returns(new NavLink());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetNavLinkById(1);

            Assert.IsType(typeof(NotFoundResult), res);
            mockRepo.Verify(r => r.FindNavLink(1));
        }

        [Fact]
        public void DeleteNavLink_ValidId_ReturnsNoContent()
        {
            var link = new NavLink()
            {
                Id = 1,
                PageId = 1,
                Title = null
            };
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindNavLink(link.Id)).Returns(link);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.DeleteNavLink(link.Id);

            Assert.IsType(typeof(NoContentResult), res);
            mockRepo.Verify(r => r.FindNavLink(link.Id));
            mockRepo.Verify(x => x.RemoveNavLink(link.Id));
        }

        [Fact]
        public void DeleteNavLink_InvalidId_ReturnsNotFound()
        {
            int id = 1;
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.FindNavLink(id)).Returns(new NavLink());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.DeleteNavLink(id);

            Assert.IsType(typeof(NotFoundResult), res);
            mockRepo.Verify(r => r.FindNavLink(id));
        }

        [Fact]
        public void GetNavLinks_NonEmptyRepo_ReturnsNavLinks()
        {
            var links = new List<NavLink>();
            for (int i = 0; i < 5; ++i)
            {
                links.Add(new NavLink()
                {
                    Id = i,
                    PageId = 1,
                    Title = "title "+i
                });
            }
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetNavLinks()).Returns(links);
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetNavLinks();

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<NavLink> actualResult = (IEnumerable<NavLink>)objResult.Value;
            Assert.Equal(links, actualResult.ToList());
            mockRepo.Verify(r => r.GetNavLinks());
        }

        [Fact]
        public void GetNavLinks_EmptyRepo_ReturnsEmptyCollection()
        {
            var mockRepo = new Mock<IPageRepository>();
            mockRepo.Setup(x => x.GetNavLinks()).Returns(new List<NavLink>());
            var controller = new CmsController(mockRepo.Object);

            var res = controller.GetNavLinks();

            Assert.IsType(typeof(ObjectResult), res);
            ObjectResult objResult = res as ObjectResult;
            IEnumerable<NavLink> actualResult = (IEnumerable<NavLink>)objResult.Value;
            Assert.Equal(new List<NavLink>(), actualResult.ToList());
            mockRepo.Verify(r => r.GetNavLinks());
        }
    }
}
