﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using CmsWebApi.Models;
using System;
using Microsoft.EntityFrameworkCore;

namespace CmsWebApi.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    public class CmsController : Controller
    {
        private readonly IPageRepository repository;

        public CmsController(IPageRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet("api/pages")]
        public IActionResult GetPages(string title = "", int offset = 0, int count = 10)
        {
            IEnumerable<Page> pages = repository.GetPages();
            var pagesCount = pages.Count();
            if (count < 0 || offset < 0 || offset > pagesCount || (pagesCount != 0 && offset == pagesCount))
            {
                return BadRequest();
            }
            if (count == 0)
            {
                count = pagesCount;
            }

            if (!string.IsNullOrEmpty(title))
            {
                title = title.ToLower();
                pages = pages.Where(x => x.Title.ToLower().Contains(title));
            }

            pages = pages.Skip(offset).Take(count);
            return new ObjectResult(pages);
        }

        [HttpGet("api/pages/{id}", Name = "GetPage")]
        public IActionResult GetPageById([FromRoute] int id)
        {
            var item = repository.FindPage(id);
            if (item == null || item.Id != id)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost("api/pages")]
        public IActionResult CreatePage([FromBody]Page page)
        {
            if (page == null || string.IsNullOrEmpty(page.UrlName) || !ValidUrlName(page))
            {
                return BadRequest();
            }
            repository.Add(page);
            return CreatedAtRoute("GetPage", new { controller = "Cms", id = page.Id }, page);
        }

        [HttpPut("api/pages/{id}")]
        public IActionResult UpdatePage([FromRoute] int id, [FromBody] Page page)
        {
            if (page == null || page.Id != id || string.IsNullOrEmpty(page.UrlName) || !ValidUrlName(page))
            {
                return BadRequest();
            }

            var item = repository.FindPage(id);
            if (item == null || item.Id != id)
            {
                return NotFound();
            }

            try
            {
                repository.Update(page);
            }
            catch (Exception e)
            {
                if (!PageExists(page.Id))
                {
                    return NotFound();
                }
                else
                {
                    return BadRequest(e);
                }
            }

            return new NoContentResult();
        }

        [HttpDelete("api/pages/{id}")]
        public IActionResult DeletePage([FromRoute] int id)
        {
            var page = repository.FindPage(id);
            if (page == null || page.Id != id)
            {
                return NotFound();
            }
            try
            {
                repository.RemovePage(id);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
            return new NoContentResult();
        }

        [HttpGet("api/navlinks")]
        public IActionResult GetNavLinks()
        {
            return new ObjectResult(repository.GetNavLinks());
        }

        [HttpGet("api/navlinks/{id}", Name = "GetNavLink")]
        public IActionResult GetNavLinkById([FromRoute] int id)
        {
            var item = repository.FindNavLink(id);
            if (item == null || item.Id != id)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost("api/navlinks")]
        public IActionResult CreateNavLink([FromBody]NavLink link)
        {
            if (isInvalid(link))
            {
                return BadRequest();
            }
            repository.Add(link);
            return CreatedAtRoute("GetNavLink", new { controller = "Cms", id = link.Id }, link);
        }

        [HttpPut("api/navlinks/{id}")]
        public IActionResult UpdateNavLink([FromRoute] int id, [FromBody] NavLink link)
        {
            if (isInvalid(link))
            {
                return BadRequest();
            }

            var item = repository.FindNavLink(id);
            if (item == null || item.Id != id)
            {
                return NotFound();
            }

            repository.Update(link);
            return new NoContentResult();
        }

        [HttpDelete("api/navlinks/{id}")]
        public IActionResult DeleteNavLink([FromRoute] int id)
        {
            var link = repository.FindNavLink(id);
            if (link == null || link.Id != id)
            {
                return NotFound();
            }

            repository.RemoveNavLink(id);
            return new NoContentResult();
        }

        [HttpGet("api/related-pages")]
        public IActionResult GetRelatePages()
        {
            IEnumerable<RelatedPage> res = repository.GetRelatedPages();
            return new ObjectResult(res);
        }

        [HttpGet("api/related-pages/{pageId}/{associatedPageId}", Name = "GetRelatedPage")]
        public IActionResult GetRelatedPageById([FromRoute] int pageId, [FromRoute]int associatedPageId)
        {
            var item = repository.FindRelatedPage(pageId, associatedPageId);
            if (item == null || item.PageId != pageId || item.AssociatedPageId != associatedPageId)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost("api/related-pages")]
        public IActionResult CreateRelatedPage([FromBody]RelatedPage rp)
        {
            if (isInvalid(rp))
            {
                return BadRequest();
            }
            repository.Add(rp);
            return CreatedAtRoute("GetRelatedPage", new { controller = "Cms", pageId = rp.PageId, associatedPageId = rp.AssociatedPageId }, rp);
        }

        [HttpDelete("api/related-pages/{pageId}/{associatedPageId}")]
        public IActionResult DeleteRelatedPage([FromRoute] int pageId, [FromRoute] int associatedPageId)
        {
            var x = repository.FindRelatedPage(pageId, associatedPageId);
            if (x == null || (x.PageId != pageId && x.AssociatedPageId != associatedPageId))
            {
                return NotFound();
            }

            repository.RemoveRelatedPage(pageId, associatedPageId);
            return new NoContentResult();
        }

        private bool ValidUrlName(Page p)
        {
            var urls = repository.GetPages().Where(x => x.Id != p.Id).Select(x => x.UrlName);
            return !urls.Contains(p.UrlName);
        }

        private bool PageExists(int id)
        {
            return repository.FindPage(id)?.Id == id;
        }

        private bool isInvalid(RelatedPage rp)
        {
            if (rp == null)
            {
                return true;
            }
            bool invalidPageId = false;
            var page = repository.FindPage(rp.PageId);
            if (page == null || page.Id != rp.PageId)
            {
                invalidPageId = true;
            }
            bool invalidAssociatedPageId = false;
            var associatedPage = repository.FindPage(rp.AssociatedPageId);
            if (associatedPage == null || associatedPage.Id != rp.AssociatedPageId)
            {
                invalidAssociatedPageId = true;
            }
            bool existsSuchRelatedPage = false;
            var x = repository.FindRelatedPage(rp.PageId, rp.AssociatedPageId);
            if (x?.PageId == rp.PageId && x?.AssociatedPageId == rp?.AssociatedPageId)
            {
                existsSuchRelatedPage = true;
            }
            return invalidPageId || invalidAssociatedPageId || existsSuchRelatedPage;
        }

        private bool isInvalid(NavLink link)
        {
            if (link == null || string.IsNullOrEmpty(link.Title))
            {
                return true;
            }
            var page = repository.FindPage(link.PageId);
            if(page == null || page.Id != link.PageId)
            {
                return true;
            }
            return false;
        }
    }
}
