﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CmsWebApi.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using System.Net;

namespace CmsWebApi.Controllers
{
    public class PagesController : Controller
    {
        private readonly string _url = "http://localhost:5000/api/pages";
        private readonly HttpClient _httpClient;

        public PagesController()
        {
            _httpClient = new HttpClient();
        }

        public async Task<IActionResult> Index(string searchString = "")
        {
            var result = await _httpClient.GetAsync(_url+ $"/?title={searchString}&count=0");
            var pages = await result.Content.ReadAsStringAsync();
            return View(JsonConvert.DeserializeObject<IEnumerable<Page>>(pages));
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var result = await _httpClient.GetAsync(_url + $"/{id}");
            var page = JsonConvert.DeserializeObject<Page>(await result.Content.ReadAsStringAsync());
            if (page == null)
            {
                return NotFound();
            }
            return View(page);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Content,Description,Title,UrlName")] Page page)
        {
            if (ModelState.IsValid)
            {
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpContent content = new StringContent(JsonConvert.SerializeObject(page), Encoding.UTF8, "application/json");
                var response = await _httpClient.PostAsync(_url,content);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(page);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var result = await _httpClient.GetAsync(_url + $"/{id}");
            var page = JsonConvert.DeserializeObject<Page>(await result.Content.ReadAsStringAsync());
            if (page == null)
            {
                return NotFound();
            }
            return View(page);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,AddedDate,Content,Description,Title,UrlName")] Page page)
        {
            if (id != page.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpContent content = new StringContent(JsonConvert.SerializeObject(page), Encoding.UTF8, "application/json");
                var response = await _httpClient.PutAsync(_url + $"/{id}", content);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return NotFound();
                }
                else
                {
                    // pass exception to ui
                } 
                return RedirectToAction("Index");
            }
            return View(page);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var result = await _httpClient.GetAsync(_url + $"/{id}");
            var page = JsonConvert.DeserializeObject<Page>(await result.Content.ReadAsStringAsync());
            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var response = await _httpClient.DeleteAsync(_url + $"/{id}");

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return NotFound();
            }
            else
            {
                // pass exception to ui
            }
            return RedirectToAction("Index");
        }

        [AcceptVerbs("Get", "Post")]
        public async Task<IActionResult> VerifyUrlName(string urlName, int id)
        {
            var result = await _httpClient.GetAsync(_url);
            var json = await result.Content.ReadAsStringAsync();
            var pages = JsonConvert.DeserializeObject<IEnumerable<Page>>(json);
            var urls = pages.Where(x => x.Id != id).Select(x => x.UrlName);
            if (urls.Contains(urlName))
            {
                return Json(data: $"URL name {urlName} is already in use. Think of a differenet one.");
            }

            return Json(data: true);
        }
    }
}

