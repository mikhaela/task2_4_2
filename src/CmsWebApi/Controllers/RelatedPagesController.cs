﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CmsWebApi.Models;
using CmsWebApi.Models.ViewModels;
using CmsWebApi.Models.DTO;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Text;

namespace CmsWebApi.Controllers
{
    public class RelatedPagesController : Controller
    {
        private readonly string _url = "http://localhost:5000/api/";
        private readonly HttpClient _httpClient;

        public RelatedPagesController(IPageRepository repository)
        {
            _httpClient = new HttpClient();
        }

        public IActionResult Manage(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var result = _httpClient.GetAsync(_url + $"pages/{id.Value}").Result;
            var page = JsonConvert.DeserializeObject<Page>(result.Content.ReadAsStringAsync().Result);

            if (page?.Id != id)
            {
                return NotFound();
            }

            var result2 = _httpClient.GetAsync(_url + $"related-pages").Result;
            var json = result2.Content.ReadAsStringAsync().Result;
            var relations = JsonConvert.DeserializeObject<IEnumerable<RelatedPage>>(json);
            var relatedPageIds = relations.Where(x => x.PageId == page.Id)
                                        .Select(x => x.AssociatedPageId);

            var result3 = _httpClient.GetAsync(_url + $"pages").Result;
            var allPages = JsonConvert.DeserializeObject<IEnumerable<Page>>(result3.Content.ReadAsStringAsync().Result);
            var pages = allPages.Where(x => x.Id != page.Id).OrderBy(x => x.Title);
            var pagesModel = new List<PageViewModel>();
            foreach (var p in pages)
            {
                pagesModel.Add(new PageViewModel
                {
                    Page = p,
                    IsRelated = relatedPageIds.Contains(p.Id)
                });
            }
            PageRelatedPagesViewModel model = new PageRelatedPagesViewModel()
            {
                Page = page,
                RelatedPages = pagesModel
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Manage(PageRelatedPagesDTO model)
        {
            IEnumerable<RelatedPage> dataToAdd = findPagesToAddToRelated(model.PageId, model.RelatedPagesIds).Result;
            IEnumerable<RelatedPage> dataToRemove = findPagesToRemoveFromRelated(model.PageId, model.RelatedPagesIds).Result;
            bool successful = removeRelatedPagesAsync(dataToRemove).Result;
            successful &= addRelatedPagesAsync(dataToAdd).Result;

            if (!successful)
            {
                return Json(new { success = false, message = "An error may have occurred. Please refresh the page and make sure that changes were saved." });
            }
            return Json(new { success = true, message = "Changes were saved." });
        }

        private async Task<bool> removeRelatedPagesAsync(IEnumerable<RelatedPage> dataToRemove)
        {
            bool successful = true;
            foreach (var x in dataToRemove)
            {
                var response = await _httpClient.DeleteAsync(_url + $"/related-pages/{x.PageId}/{x.AssociatedPageId}");
                successful &= response.IsSuccessStatusCode;
            }
            return successful;
        }

        private async Task<bool> addRelatedPagesAsync(IEnumerable<RelatedPage> dataToAdd)
        {
            bool successful = true;
            foreach (var x in dataToAdd)
            {
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpContent content = new StringContent(JsonConvert.SerializeObject(x), Encoding.UTF8, "application/json");
                var response = await _httpClient.PostAsync(_url + "/related-pages", content);
                successful &= response.IsSuccessStatusCode;
            }
            return successful;
        }

        private async Task<IEnumerable<RelatedPage>> findPagesToAddToRelated(int pageId, IEnumerable<int> data)
        {
            var result = await _httpClient.GetAsync(_url + $"related-pages");
            var relations = JsonConvert.DeserializeObject<IEnumerable<RelatedPage>>(await result.Content.ReadAsStringAsync());
            var oldRelatedPages = relations.Where(x => x.PageId == pageId).Select(x => x.AssociatedPageId);
            if (oldRelatedPages == null && data != null)
            {
                return buildRelatedPages(pageId, data);
            }
            if (oldRelatedPages != null && data == null)
            {
                return new List<RelatedPage>();
            }
            else
            {
                return buildRelatedPages(pageId, data.Except(oldRelatedPages));
            }
        }

        private async Task<IEnumerable<RelatedPage>> findPagesToRemoveFromRelated(int pageId, IEnumerable<int> data)
        {
            var result = await _httpClient.GetAsync(_url + $"related-pages");
            var relations = JsonConvert.DeserializeObject<IEnumerable<RelatedPage>>(await result.Content.ReadAsStringAsync());
            var oldRelatedPages = relations.Where(x => x.PageId == pageId).Select(x => x.AssociatedPageId);
            if (oldRelatedPages == null && data != null)
            {
                return new List<RelatedPage>();
            }
            if (oldRelatedPages != null && data == null)
            {
                return buildRelatedPages(pageId, oldRelatedPages);
            }
            else
            {
                return buildRelatedPages(pageId, oldRelatedPages.Except(data));
            }
        }

        private IEnumerable<RelatedPage> buildRelatedPages(int pageId, IEnumerable<int> associatedPageIds)
        {
            var result = new List<RelatedPage>();
            if (associatedPageIds != null)
            {
                foreach (var id in associatedPageIds)
                {
                    result.Add(new RelatedPage()
                    {
                        PageId = pageId,
                        AssociatedPageId = id
                    });
                }
            }
            return result;
        }
    }
}
