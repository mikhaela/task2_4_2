﻿using Microsoft.EntityFrameworkCore;
using CmsWebApi.Models;

namespace CmsWebApi.Data
{
    public class CmsDbContext : DbContext
    {
        public CmsDbContext(DbContextOptions<CmsDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=./cms.db");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Page>()
                .Property(e => e.AddedDate)
                .HasDefaultValueSql("current_timestamp").ValueGeneratedOnAdd();

            builder.Entity<RelatedPage>()
                .HasKey(r => new { r.PageId, r.AssociatedPageId });

            builder.Entity<RelatedPage>()
               .HasOne(rp => rp.Page)
               .WithMany(p => p.Pages)
               .HasForeignKey(rp => rp.PageId);

            builder.Entity<RelatedPage>()
                .HasOne(rp => rp.AssociatedPage)
                .WithMany(p => p.AssociatedPages)
                .HasForeignKey(rp => rp.AssociatedPageId);

            builder.Entity<Page>()
                .HasIndex(p => p.UrlName)
                .IsUnique();
        }

        public DbSet<Page> Pages { get; set; }
        public DbSet<RelatedPage> RelatedPages { get; set; }
        public DbSet<NavLink> NavLinks { get; set; }
    }
}
