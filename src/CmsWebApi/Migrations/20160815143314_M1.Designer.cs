﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CmsWebApi.Data;

namespace CmsWebApi.Migrations
{
    [DbContext(typeof(CmsDbContext))]
    [Migration("20160815143314_M1")]
    partial class M1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("CmsWebApi.Models.NavLink", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("PageId");

                    b.Property<int?>("ParentLinkId");

                    b.Property<int>("Position");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("varchar(200)");

                    b.HasKey("Id");

                    b.HasIndex("PageId");

                    b.HasIndex("ParentLinkId");

                    b.ToTable("NavLinks");
                });

            modelBuilder.Entity("CmsWebApi.Models.Page", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("current_timestamp");

                    b.Property<string>("Content")
                        .HasColumnType("text");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(250)");

                    b.Property<string>("Title")
                        .HasColumnType("varchar(200)");

                    b.Property<string>("UrlName")
                        .IsRequired()
                        .HasColumnType("varchar(200)");

                    b.HasKey("Id");

                    b.HasIndex("UrlName")
                        .IsUnique();

                    b.ToTable("Pages");
                });

            modelBuilder.Entity("CmsWebApi.Models.RelatedPage", b =>
                {
                    b.Property<int>("PageId");

                    b.Property<int>("AssociatedPageId");

                    b.HasKey("PageId", "AssociatedPageId");

                    b.HasIndex("AssociatedPageId");

                    b.HasIndex("PageId");

                    b.ToTable("RelatedPages");
                });

            modelBuilder.Entity("CmsWebApi.Models.NavLink", b =>
                {
                    b.HasOne("CmsWebApi.Models.Page", "Page")
                        .WithMany()
                        .HasForeignKey("PageId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CmsWebApi.Models.NavLink", "ParentLink")
                        .WithMany()
                        .HasForeignKey("ParentLinkId");
                });

            modelBuilder.Entity("CmsWebApi.Models.RelatedPage", b =>
                {
                    b.HasOne("CmsWebApi.Models.Page", "AssociatedPage")
                        .WithMany("AssociatedPages")
                        .HasForeignKey("AssociatedPageId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CmsWebApi.Models.Page", "Page")
                        .WithMany("Pages")
                        .HasForeignKey("PageId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
