﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CmsWebApi.Migrations
{
    public partial class M1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Autoincrement", true),
                    AddedDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "current_timestamp"),
                    Content = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "varchar(250)", nullable: true),
                    Title = table.Column<string>(type: "varchar(200)", nullable: true),
                    UrlName = table.Column<string>(type: "varchar(200)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NavLinks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Autoincrement", true),
                    PageId = table.Column<int>(nullable: false),
                    ParentLinkId = table.Column<int>(nullable: true),
                    Position = table.Column<int>(nullable: false),
                    Title = table.Column<string>(type: "varchar(200)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NavLinks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NavLinks_Pages_PageId",
                        column: x => x.PageId,
                        principalTable: "Pages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NavLinks_NavLinks_ParentLinkId",
                        column: x => x.ParentLinkId,
                        principalTable: "NavLinks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RelatedPages",
                columns: table => new
                {
                    PageId = table.Column<int>(nullable: false),
                    AssociatedPageId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RelatedPages", x => new { x.PageId, x.AssociatedPageId });
                    table.ForeignKey(
                        name: "FK_RelatedPages_Pages_AssociatedPageId",
                        column: x => x.AssociatedPageId,
                        principalTable: "Pages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RelatedPages_Pages_PageId",
                        column: x => x.PageId,
                        principalTable: "Pages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NavLinks_PageId",
                table: "NavLinks",
                column: "PageId");

            migrationBuilder.CreateIndex(
                name: "IX_NavLinks_ParentLinkId",
                table: "NavLinks",
                column: "ParentLinkId");

            migrationBuilder.CreateIndex(
                name: "IX_Pages_UrlName",
                table: "Pages",
                column: "UrlName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RelatedPages_AssociatedPageId",
                table: "RelatedPages",
                column: "AssociatedPageId");

            migrationBuilder.CreateIndex(
                name: "IX_RelatedPages_PageId",
                table: "RelatedPages",
                column: "PageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NavLinks");

            migrationBuilder.DropTable(
                name: "RelatedPages");

            migrationBuilder.DropTable(
                name: "Pages");
        }
    }
}
