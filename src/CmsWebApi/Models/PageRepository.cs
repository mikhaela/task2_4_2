﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using CmsWebApi.Data;

namespace CmsWebApi.Models
{
    public class PageRepository : IPageRepository
    {
        private static CmsDbContext _context = new CmsDbContext(new DbContextOptions<CmsDbContext>());

        public void Add(NavLink link)
        {
            link.Position = link.ParentLinkId == null ? 0 : _context.NavLinks
                                                .Single(n => n.Id == (int)link.ParentLinkId)
                                                .Position + 1;
            _context.NavLinks.Add(link);
            _context.SaveChanges();
            _context.Entry(link).State = EntityState.Detached;
        }

        public void Add(RelatedPage rp)
        {
            _context.RelatedPages.Add(rp);
            _context.SaveChanges();
            _context.Entry(rp).State = EntityState.Detached;
        }

        public void Add(Page page)
        {
            _context.Pages.Add(page);
            _context.SaveChanges();
            _context.Entry(page).State = EntityState.Detached;
        }

        public NavLink FindNavLink(int id)
        {
            return _context.NavLinks
                    .AsNoTracking()
                    .SingleOrDefault(x => x.Id == id);
        }

        public Page FindPage(int id)
        {
            return _context.Pages.AsNoTracking().SingleOrDefault(x => x.Id == id);
        }

        public RelatedPage FindRelatedPage(int pageId, int associatePageId)
        {
            return _context.RelatedPages.AsNoTracking().SingleOrDefault(x => x.PageId == pageId && x.AssociatedPageId == associatePageId);
        }

        public IEnumerable<NavLink> GetNavLinks()
        {
            return _context.NavLinks
                        .Include(x => x.Page)
                        .AsNoTracking();
        }

        public IEnumerable<Page> GetPages()
        {
            return _context.Pages.AsNoTracking();
        }

        public IEnumerable<RelatedPage> GetRelatedPages()
        {
            var res = _context.RelatedPages
                        //.Include(x => x.AssociatedPage)
                        .AsNoTracking();
            return res;
        }

        public void RemoveNavLink(int id)
        {
            var link = _context.NavLinks.SingleOrDefault(m => m.Id == id);
            _context.NavLinks.Remove(link);
            _context.SaveChanges();
        }

        public void RemovePage(int id)
        {
            var page = _context.Pages.SingleOrDefault(m => m.Id == id);
            _context.Pages.Remove(page);
            _context.SaveChanges();
        }

        public void RemoveRelatedPage(int pageId, int associatePageId)
        {
            var rp = _context.RelatedPages.SingleOrDefault(x => x.PageId == pageId && x.AssociatedPageId == associatePageId);
            _context.RelatedPages.Remove(rp);
            _context.SaveChanges();
        }

        public void Update(NavLink link)
        {
            link.Position = link.ParentLinkId == null ? 0 : _context.NavLinks
                                    .Single(n => n.Id == (int)link.ParentLinkId)
                                    .Position + 1;
            _context.NavLinks.Update(link);
            _context.SaveChanges();
            _context.Entry(link).State = EntityState.Detached;
        }

        public void Update(RelatedPage rp)
        {
            _context.RelatedPages.Update(rp);
            _context.SaveChanges();
            _context.Entry(rp).State = EntityState.Detached;
        }

        public void Update(Page page)
        {
            _context.Pages.Update(page);
            _context.SaveChanges();
            _context.Entry(page).State = EntityState.Detached;
        }
    }
}
