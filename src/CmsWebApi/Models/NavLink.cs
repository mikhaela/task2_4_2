﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CmsWebApi.Models
{
    public class NavLink
    {
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(200)")]
        public string Title { get; set; }
        public int Position { get; set; }

        public int? ParentLinkId { get; set; }
        public NavLink ParentLink { get; set; }

        public int PageId { get; set; }
        public Page Page { get; set; }
    }
}
