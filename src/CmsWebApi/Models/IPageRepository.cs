﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CmsWebApi.Models
{
    public interface IPageRepository
    {
        void Add(Page page);
        void Add(RelatedPage rp);
        void Add(NavLink link);

        IEnumerable<Page> GetPages();
        IEnumerable<RelatedPage> GetRelatedPages();
        IEnumerable<NavLink> GetNavLinks();

        Page FindPage(int id);
        RelatedPage FindRelatedPage(int pageId, int associatePageId);
        NavLink FindNavLink(int id);

        void RemovePage(int id);
        void RemoveRelatedPage(int pageId, int associatePageId);
        void RemoveNavLink(int id);

        void Update(Page page);
        void Update(RelatedPage rp);
        void Update(NavLink link);
    }
}
