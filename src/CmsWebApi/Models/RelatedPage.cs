﻿namespace CmsWebApi.Models
{
    public class RelatedPage
    {
        public int PageId { get; set; }
        public Page Page { get; set; }

        public int AssociatedPageId { get; set; }
        public Page AssociatedPage { get; set; }
    }
}
