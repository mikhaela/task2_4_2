﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CmsWebApi.Models
{
    public class Page
    {
        public int Id { get; set; }

        [Required, Column(TypeName = "varchar(200)")]
        [Remote(action: "VerifyUrlName", controller: "Pages", AdditionalFields = "Id")]
        [Display(Name = "URL Name")]
        public string UrlName { get; set; }


        [Column(TypeName = "varchar(200)")]
        public string Title { get; set; }

        [Column(TypeName = "varchar(250)")]
        public string Description { get; set; }

        [Column(TypeName = "text")]
        public string Content { get; set; }

        [Column(TypeName = "datetime")]
        [DataType(DataType.Date)]
        [Display(Name = "Added on")]
        public DateTime AddedDate { get; set; }


        public List<RelatedPage> Pages { get; set; }

        public List<RelatedPage> AssociatedPages { get; set; }
    }
}
