﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CmsWebApi.Models.DTO
{
    public class PageRelatedPagesDTO
    {
        public int PageId { get; set; }
        public int[] RelatedPagesIds { get; set; }
    }
}
