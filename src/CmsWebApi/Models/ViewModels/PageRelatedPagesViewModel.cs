﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CmsWebApi.Models.ViewModels
{
    public class PageRelatedPagesViewModel
    {
        public Page Page { get; set; }
        public List<PageViewModel> RelatedPages { get; set; }
    }
}
