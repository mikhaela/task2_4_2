﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CmsWebApi.Models.ViewModels
{
    public class PageViewModel
    {
        public Page Page { get; set; }
        public bool IsRelated { get; set; }
    }
}
