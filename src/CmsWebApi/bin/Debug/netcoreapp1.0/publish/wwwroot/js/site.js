﻿tinymce.init({
    selector: ".htmlContent"
});

$(function () {
  $("#pages").tablesorter();
});


$( document ).ready(function() {
    $('.page-btn').first().addClass('btn-primary');

    $("tr[class^='page-']").each(function(){
        $(this).hide();
    });

    $('.page-1').show();

    $('.page-btn').click(function(){
        var pageToDisplay = $(this).text();
        
        $('.page-btn').each(function(){
            $(this).removeClass('btn-primary');
        });
        $(this).addClass('btn-primary');

        $("tr[class^='page-']").each(function(){
            $(this).hide();
        });
        $(".page-"+pageToDisplay).each(function(){
            $(this).show();
        });
    });

    $('#save-related').click(function () {
        $('#feedback').css("display", "none");
        var related = [];
        $("div[name='related']").each(function () {
            if($(this).children().first().is(':checked')) {
                related.push($(this).attr("id"));
            }
        });
         var p = $('#page').attr("value");
        $.ajax({
            url: "/RelatedPages/Manage",
            type: "POST",
            data: {pageId:p, relatedPagesIds:related},
            dataType: "json",
            traditional:true,
            success: function(result) {
                if(result.success) {
                    $('#feedback').addClass("alert-success");
                    $('#feedback').text(result.message);
                    $('#feedback').css("display", "block");
                } else {
                    $('#feedback').addClass("alert-danger");
                    $('#feedback').text(result.message);
                    $('#feedback').css("display", "block");
                }
            }
        });
    });
  });


